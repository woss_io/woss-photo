/* eslint-disable no-undef */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-var-requires */
const path = require(`path`)
const { createFilePath } = require(`gatsby-source-filesystem`)
const fastExif = require('fast-exif')
const CID = require('cids')
const mh = require('multihashing-async')
const exiftool = require('exiftool-vendored').exiftool
const { readFileSync } = require('fs')

exports.createPages = async ({ graphql, actions, reporter }) => {
  const { createPage } = actions
  return graphql(`
    {
      allFile(filter: { sourceInstanceName: { eq: "photos" } }) {
        edges {
          node {
            fields {
              cid
              imageUrlPath
            }
            absolutePath
            id
          }
        }
      }
    }
  `).then((result) => {
    if (result.errors) {
      console.log(result.errors)
    }
    const imagePageTemplate = path.resolve('./src/templates/Photo.tsx')

    result.data.allFile.edges.forEach(({ node }) => {
      createPage({
        path: node.fields.imageUrlPath,
        component: imagePageTemplate,
        context: {
          cid: node.fields.cid,
        },
      })
    })
  })
}

exports.onCreateNode = ({ node, getNode, actions }) => {
  const { createNodeField } = actions

  if (node.sourceInstanceName === `photos` && node.internal.type === `File`) {
    mh(readFileSync(node.absolutePath), 'blake2b-512')
      .then((hash) => {
        const cid = new CID(1, 'dag-pb', hash)
        createNodeField({
          node,
          name: `cid`,
          value: cid.toString(),
        })
        createNodeField({
          node,
          name: `imageUrlPath`,
          value: `/photo/${cid.toString()}`,
        })
      })
      .catch(console.error)

    exiftool
      .read(node.absolutePath)
      .then((tags /*: Tags */) => {
        /**
         * Fix the inconsistency when the values are natural number like -21 or +32 and JS cannot handle that
         *
         * If there is a single keyword it will be a string, any other case a list of strings
         */
        const value = Object.assign({}, tags, {
          Keywords: Array.isArray(tags.Keywords) ? tags.Keywords : [tags.Keywords],
          Contrast2012: tags.Contrast2012?.toString(),
          Highlights2012: tags.Highlights2012?.toString(),
          Shadows2012: tags.Shadows2012?.toString(),
          Blacks2012: tags.Blacks2012?.toString(),
          Clarity2012: tags.Clarity2012?.toString(),
          Vibrance: tags.Vibrance?.toString(),
          Whites2012: tags.Whites2012?.toString(),
          Texture: tags.Texture?.toString(),
          Dehaze: tags.Dehaze?.toString(),
          Saturation: tags.Saturation?.toString(),
          Sharpness: tags.Sharpness?.toString(),
          HueAdjustmentAqua: tags.HueAdjustmentAqua?.toString(),
          HueAdjustmentBlue: tags.HueAdjustmentBlue?.toString(),
          HueAdjustmentPurple: tags.HueAdjustmentPurple?.toString(),
          ExposureTime: tags.ExposureTime?.toString(),
          ShutterSpeedValue: tags.ShutterSpeedValue?.toString(),
          ExposureCompensation: tags.ExposureCompensation?.toString(),
          SubSecTimeDigitized: tags.SubSecTimeDigitized?.toString(),
          SubSecTimeOriginal: tags.SubSecTimeOriginal?.toString(),
          IncrementalTint: tags.IncrementalTint?.toString(),
          ShutterSpeed: tags.ShutterSpeed?.toString(),
          Title: tags.Title ? tags.Title : 'Untitled',
          Caption: tags.Title ? tags['Caption-Abstract'] : null,
          CreatedDate: tags.CreateDate.toString(),
        })
        createNodeField({
          node,
          name: `metadata`,
          value,
        })
        // console.log(tags.CreateDate.toString(), tags.DateCreated.toString(), tags.DateTimeCreated.toString())
      })
      .catch((err) => console.error('Something terrible happened: ', err))
  }
}

// exports.onCreateWebpackConfig = ({ actions, loaders, stage }) => {
//   if (stage === "build-html") {
//     actions.setWebpackConfig({
//       module: {
//         rules: [
//           {
//             test: /mapbox-gl/,
//             use: loaders.null(),
//           },
//         ],
//       }
//     })
//   }
// };
