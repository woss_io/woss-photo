#!/usr/bin/env bash

certbot renew \
--logs-dir /home/daniel/projects/woss/woss.photo/letsencrypt/log/ \
--config-dir /home/daniel/projects/woss/woss.photo/letsencrypt/config/ \
--work-dir /home/daniel/projects/woss/woss.photo/letsencrypt/work/ \
--non-interactive \
--server https://acme-v02.api.letsencrypt.org/directory
