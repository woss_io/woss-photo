// eslint-disable-next-line no-undef
module.exports = {
  pathPrefix: `/woss_io`,
  siteMetadata: {
    title: 'Photo Portfolio',
    author: {
      name: 'Daniel Maricic',
      summary: 'My portfolio',
    },
    description: 'My wanderings while capturing the weird and beautiful moments',
    siteUrl: 'https://woss.photo',
    social: {
      twitter: 'woss_io',
      gitlab: 'https://gitlab.com/woss_io/woss-photo',
    },
  },
  flags: {
    // FUNCTIONS: true,
    FAST_DEV: true,
    DEV_SSR: true,
    PRESERVE_WEBPACK_CACHE: true,
    PARALLEL_SOURCING: true,
  },
  plugins: [
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `photos`,
        // eslint-disable-next-line no-undef
        path: `${__dirname}/src/content/photos/`,
      },
    },
    'gatsby-plugin-react-helmet',
    `gatsby-plugin-image`,
    {
      resolve: `gatsby-plugin-sharp`,
      options: {
        // Defaults used for gatsbyImageData and StaticImage
        // defaults: {},
        // Set to false to allow builds to continue on image errors
        // failOnError: true,
        // deprecated options and their defaults:
        // base64Width: 20,
        // forceBase64Format: ``, // valid formats: png,jpg,webp
        // useMozJpeg: process.env.GATSBY_JPEG_ENCODER === `MOZJPEG`,
        stripMetadata: false,
        defaultQuality: 100,
      },
    },
    `gatsby-transformer-sharp`, // Needed for dynamic images
    `gatsby-plugin-pnpm`,
    {
      resolve: `gatsby-plugin-s3`,
      options: {
        bucketName: 'woss.photo',
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        icon: `src/images/logo.png`, // This path is relative to the root of the site.
        name: `Woss Photo`,
        short_name: `woss-photo`,
        start_url: `/`,
        background_color: `#f7f0eb`,
        theme_color: `#a2466c`,
        display: `standalone`,
      },
    },
    'gatsby-plugin-offline'

  ],
}
