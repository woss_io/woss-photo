#!/bin/sh

# export AWS_ACCESS_KEY_ID="<REPLACE_WITH_YOUR_KEY>"
# export AWS_SECRET_ACCESS_KEY="<REPLACE_WITH_YOUR_SECRET>"

certbot --agree-tos -a certbot-s3front:auth \
--certbot-s3front:auth-s3-bucket woss.photo \
--certbot-s3front:auth-s3-region eu-west-1 \
-i certbot-s3front:installer \
--certbot-s3front:installer-cf-distribution-id E18Q2VBCYHABEJ \
-d woss.photo
