import { Header } from 'grommet'
import React from 'react'
export default function HeaderLayout({ children }) {
  return (
    <Header background="background" pad="medium" height="xsmall">
      {children}
    </Header>
  )
}
