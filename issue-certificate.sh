#!/usr/bin/env bash

certbot certonly \
-d woss.photo \
--dns-route53 \
--preferred-challenges=dns \
--logs-dir /home/daniel/projects/woss/woss.photo/letsencrypt/log/ \
--config-dir /home/daniel/projects/woss/woss.photo/letsencrypt/config/ \
--work-dir /home/daniel/projects/woss/woss.photo/letsencrypt/work/ \
-m daniel@woss.io \
--agree-tos \
--non-interactive \
--server https://acme-v02.api.letsencrypt.org/directory
